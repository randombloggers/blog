---
title: For A New Beginning
author: Bing
categories: []
abbrlink: f6b5df8d
date: 2024-01-18 22:30:36
---

For A New Beginning

By John O’Donohue

 

In out-of-the-way places of the heart,

Where your thoughts never think to wander,

This beginning has been quietly forming,

Waiting until you were ready to emerge.

 

For a long time it has watched your desire,

Feeling the emptiness growing inside you,

Noticing how you willed yourself on,

Still unable to leave what you had outgrown.



It watched you play with the seduction of safety

And the gray promises that sameness whispered,

Heard the waves of turmoil rise and relent,

Wondered would you always live like this.

 

Then the delight, when your courage kindled,

And out you stepped onto new ground,

Your eyes young again with energy and dream,

A path of plenitude opening before you.

 

Though your destination is not yet clear

You can trust the promise of this opening;

Unfurl yourself into the grace of beginning

That is at one with your life’s desire.

 

Awaken your spirit to adventure;

Hold nothing back, learn to find ease in risk;

Soon you will home in a new rhythm,

For your soul senses the world.

 

为一个新的开始

约翰·奥多诺

 

在心脏的偏僻处，

在你的思想从未想过游荡的地方，

这个开端已经悄然形成，

等待，直到你准备好出现。

很长一段时间以来，它一直关注着你的欲望，

感觉到你内心的空虚，

注意到你的意志，

仍然无法离开你已经长大的东西。

它看着你在安全的诱惑下玩耍

灰色的承诺，那千篇一律的低语，

听到骚动的浪潮兴起并缓和下来，

真想知道你会一直这样生活吗。

然后喜悦，当你的勇气点燃，

然后你踏上了新的天地，

你的眼睛再次充满活力和梦想，

一条充实的道路在你面前打开。

虽然你的目的地还不清楚

你可以相信这个开场白的承诺；

让自己沉浸在开始的优雅中

这与你的生活愿望是一致的。

唤醒你的冒险精神；

不要退缩，学会在风险中找到放松；

很快你就会以新的节奏回家，

因为你的灵魂感知世界