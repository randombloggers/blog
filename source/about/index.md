---
title: Of The Bloggers
date: 2024-01-18 22:05:36
comments: false
cover: false
aside: false
highlight_shrink: true
description: 关于站点的简单介绍
---

## 简介
本站是由几位撰写者共同维护的博客，始于2024/1/18。
## 特色
本博客的模板文件为
```md front-matter
---
title: {{title}}
abbrlink: unnamedPost
categories: 
tags: []
date: {{date:YYYY-MM-DD HH:mm:ss}}
updated: {{date:YYYY-MM-DD HH:mm:ss}}
cover: 
root: ../../
author: 
comments: 
katex: false
password: 
theme: xray
hidden: 
published: true
description: 
---

```
### 支持的功能
本站最初使用npm建立，之后主要通过更快速的`pnpm`进行维护：

```shell
pnpm install ... #安装npm包
pnpm update ... #更新npm包
```
#### 文章隐蔽
采用**hexo-hide**插件，在需要隐蔽的front-matter（一下通称为“导言区”）中`hidden`添加`true`（隐蔽文章）、`false`（公开文章）。当不添加值时，也就是默认值为**公开文章**。

### 文章加密
采用**hexo-blog-encrypt**插件，在导言区`password`字段添加字符即可加密。

### 文章样式
本博客主体采用**hexo-renderer-markdown-it-plus**进行渲染，并添加了如下插件来增加了新功能

* markdown-it-mark
* markdown-it-abbr
* markdown-it-footnote(尾注)
* markdown-it-ins
* markdown-it-sub（上标）
* markdown-it-sup（下标）
* markdown-it-attrs
* markdown-it-cjk-breaks
* markdown-it-container
* markdown-it-task-lists（任务列表）
* '@neilsustc/markdown-it-katex'（$KaTeX$数学语法）

#### hexo通用标签
为了方便主题的转移，我们使用插件提供的标签来增加更多样式，主要有
##### 思维导图


##### hexo-butterfly-tag-plugins-plus
配套vscode插件(id: youngjuning.hexo-butterfly-tag-plugins-plus-snippets)进行编辑。












## 加入

